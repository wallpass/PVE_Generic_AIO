

#### 安装图形界面：
```
tasksel
```

#### 创建桌面用户：

```
#创建用户
useradd -m [新用户名] -s /bin/bash 
#修改密码
passwd [新用户名]
```

#### 安装 Kodi：
```
apt install kodi -y
```

#### 【非必须】12代CPU或显卡无法驱动可先更新核心到5.17版本
参考：https://github.com/fabianishere/pve-edge-kernel
```
curl -1sLf 'https://dl.cloudsmith.io/public/pve-edge/kernel/gpg.8EC01CCF309B98E7.key' | apt-key add -
echo "deb https://dl.cloudsmith.io/public/pve-edge/kernel/deb/debian bullseye main" > /etc/apt/sources.list.d/pve-edge-kernel.list
apt update
apt install pve-kernel-5.17.3-edge -y
```

#### 【非必须】如还无法播放，请更新最新版intel_va驱动（12代必须安装_更换不稳定源可能影响PVE稳定性）

将stable源更换为sid源
```
nano /etc/apt/sources.list
```

安装新版驱动
```
apt update
apt install  intel-media-va-driver-non-free -y
```

将sid源更换回stable源
```
nano /etc/apt/sources.list
```

更新索引
```
apt update
```
#### 设置自动登录 & 免休眠